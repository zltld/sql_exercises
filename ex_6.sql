/*For each maker producing laptops with a hard drive capacity of 10 Gb or higher, find the speed of such laptops. Result set: maker, speed. */

SELECT DISTINCT p.maker, l.speed 
FROM product p
INNER JOIN laptop l on p.model = l.model
WHERE l.hd >= 10
